package com.honstain.inventory.resources;

import com.codahale.metrics.annotation.Timed;
import com.honstain.inventory.api.Inventory;
import com.honstain.inventory.api.InventoryTransfer;
import com.honstain.inventory.api.Pricing;
import com.honstain.inventory.api.ShipCost;
import com.honstain.inventory.db.InventoryDao;
import com.honstain.inventory.db.PricingDao;
import com.honstain.inventory.db.ShipCostDao;
import org.jdbi.v3.core.Jdbi;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import java.util.*;

@Path("/")
@Produces(MediaType.APPLICATION_JSON)
public class InventoryResource {

    public final Jdbi jdbi;
    public final InventoryDao dao;
    public final PricingDao pricingDao;
    public final ShipCostDao shipCostDao;

    public InventoryResource(Jdbi jdbi) {
        this.jdbi = jdbi;
        this.dao = jdbi.onDemand(InventoryDao.class);
        this.pricingDao = jdbi.onDemand(PricingDao.class);
        this.shipCostDao = jdbi.onDemand(ShipCostDao.class);
    }

    @GET
    @Timed
    public List<Inventory> getAllInventory() {
        List<Inventory> result = this.dao.selectAll();
        result.sort(Comparator.comparing(Inventory::getLocation).thenComparing(Inventory::getSku));
        //System.out.println(result);
        return result;
    }

    @GET
    @Timed
    @Path("/{location}")
    public List<Inventory> getInventoryByLocation(@PathParam("location") String location) {
        List<Inventory> result = this.dao.selectByLocation(location);
        //System.out.println(result);
        return result;
    }

    @POST
    @Timed
    public Inventory createUpdateInventory(Inventory inventory) {
        UUID newID = UUID.randomUUID();

        Inventory result = jdbi.inTransaction(h -> {
            Inventory updatedRecord;
            List<Inventory> existing = this.dao.lockAndSelectByLocationAndSku(inventory.getLocation(), inventory.getSku());
            if (!existing.isEmpty()) {
                Inventory existingRecord = existing.get(0);
                this.dao.updateQty(existingRecord.getId(), existingRecord.getLocation(), existingRecord.getSku(), inventory.getQty());
                updatedRecord = new Inventory(existingRecord.getId(), existingRecord.getLocation(), existingRecord.getSku(), inventory.getQty());
            } else {
                this.dao.insert(newID, inventory.getLocation(), inventory.getSku(), inventory.getQty());
                updatedRecord = new Inventory(newID, inventory.getLocation(), inventory.getSku(), inventory.getQty());
            }
            return updatedRecord;
        });

        return result;
    }

    @POST
    @Timed
    @Path("/transfer")
    public void transferInventory(InventoryTransfer transfer) {
        /*
        TODO - I need to make a test for this, also all of this code shouldn't live in the resource.

        This was mostly just to scratch an itch and compare this logic to my Django and Scala
        implementations for the purposes of refreshing myself with Java.
         */
        final String sku = transfer.getSku();

        jdbi.inTransaction(h -> {
            String firstLockLocation;
            String secondLockLocation;
            if (transfer.getFromLocation().compareTo(transfer.getToLocation()) < 0) {
                firstLockLocation = transfer.getToLocation();
                secondLockLocation = transfer.getFromLocation();
            } else {
                firstLockLocation = transfer.getFromLocation();
                secondLockLocation = transfer.getToLocation();
            }
            this.dao.lockAndSelectByLocationAndSku(firstLockLocation, sku);
            this.dao.lockAndSelectByLocationAndSku(secondLockLocation, sku);

            List<Inventory> fromLocations = this.dao.lockAndSelectByLocationAndSku(transfer.getFromLocation(), sku);
            if (!fromLocations.isEmpty()) {
                Inventory fromLocation = fromLocations.get(0);
                int newQty = fromLocation.getQty() - transfer.getQty();
                if (newQty < 0) {
                    throw new RuntimeException("Insufficient quantity");
                }
                this.dao.updateQty(fromLocation.getId(), fromLocation.getLocation(), sku, newQty);
            } else {
                // There must be a from location
                throw new RuntimeException("Invalid location to transfer inventory from");
            }

            List<Inventory> toLocations = this.dao.lockAndSelectByLocationAndSku(transfer.getToLocation(), sku);
            if (!toLocations.isEmpty()) {
                Inventory toLocation = toLocations.get(0);
                int newQty = toLocation.getQty() + transfer.getQty();
                this.dao.updateQty(toLocation.getId(), toLocation.getLocation(), sku, newQty);
            } else {
                this.dao.insert(UUID.randomUUID(), transfer.getToLocation(), sku, transfer.getQty());
            }
            return null;
        });
    }

    @GET
    @Timed
    @Path("/pricing")
    public void calculatePricing() {
        List<String> requestedSkus = List.of("SKU-01", "SKU-02", "SKU-03", "SKU-04");

        List<Pricing> pricingList = new ArrayList<>();
        List<ShipCost> shipCostList = new ArrayList<>();
        for (String sku: requestedSkus) {
            List<Pricing> pricingListBySku = this.pricingDao.selectBySku(sku);
            pricingList.addAll(pricingListBySku);

            List<ShipCost> shipCostBySku = this.shipCostDao.selectBySku(sku);
            shipCostList.addAll(shipCostBySku);
        }

        // TODO - Problem is that we will have to many ship costs records
        // Some of the results define invalid relationships
        // How do I model a set here? Where should I move the logic?
        // TODO - Out of the gate, I only look up data I have in the DB, nothing to filter yet

        // Start with greedy - ignore shipping cost.
        List<Pricing> minPrice = getMinPricing(requestedSkus, pricingList);

        Map<Pricing, ShipCost> shipCostMap = getMinShipping(minPrice, shipCostList);
        int total = 0;
        for (Pricing price: shipCostMap.keySet()) {
            ShipCost shipCost = shipCostMap.get(price);
            System.out.println(String.format("Merchant: %d SKU: %s Cost: %d ShipCost: %d",
                    price.getMerchant(),
                    price.getSku(),
                    price.getCost(),
                    shipCost.getCost()));
            total += price.getCost() + shipCost.getCost();
        }
        System.out.println("Total:" + total);
    }

    public List<Pricing> getMinPricing(List<String> requestedSkus, List<Pricing> pricingList) {
        List<Pricing> result = new ArrayList<>();
        for (String sku: requestedSkus) {
            Pricing minPrice = null;
            for (Pricing pricing: pricingList) {
                if (pricing.getSku().equals(sku)) {
                    if (minPrice == null) {
                        minPrice = pricing;
                    } else if (pricing.getCost() < minPrice.getCost()) {
                        minPrice = pricing;
                    }
                }
            }
            result.add(minPrice);
        }
        return result;
    }

    public Map<Pricing, ShipCost> getMinShipping(
            List<Pricing> pricingList,
            List<ShipCost> shipCostList) {
        Map<Pricing, ShipCost> result = new HashMap<>();
        for (Pricing pricing: pricingList) {
            ShipCost minCost = null;
            for (ShipCost shipCost: shipCostList) {
                if (pricing.getSku().equals(shipCost.getSku()) && pricing.getMerchant().equals(shipCost.getMerchant())) {
                    if (minCost == null) {
                        minCost = shipCost;
                    } else if (pricing.getCost() < minCost.getCost()) {
                        minCost = shipCost;
                    }
                }
            }
            result.put(pricing, minCost);
        }
        return result;
    }
}
