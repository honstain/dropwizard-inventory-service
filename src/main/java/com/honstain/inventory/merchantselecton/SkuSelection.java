package com.honstain.inventory.merchantselecton;

import com.honstain.inventory.api.Pricing;
import com.honstain.inventory.api.ShipCost;

public class SkuSelection {
    private String sku;
    private Pricing pricing;
    private ShipCost shipCost;

    public SkuSelection(String sku, Pricing pricing, ShipCost shipCost) {
        this.sku = sku;
        this.pricing = pricing;
        this.shipCost = shipCost;
    }

    public String getSku() {
        return sku;
    }

    public Pricing getPricing() {
        return pricing;
    }

    public ShipCost getShipCost() {
        return shipCost;
    }
}
