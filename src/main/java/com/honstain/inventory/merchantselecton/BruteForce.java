package com.honstain.inventory.merchantselecton;

import com.honstain.inventory.api.Pricing;
import com.honstain.inventory.api.ShipCost;
import org.antlr.v4.runtime.misc.Array2DHashSet;

import java.util.*;

public class BruteForce {

    public static Set<List<String>> deepCopy(Set<List<String>> current) {
        Set<List<String>> result = new HashSet<>();

        for (List<String> subArray: current) {
            List<String> temp = new ArrayList<>();
            for (String element: subArray) {
                temp.add(element);
            }
            result.add(temp);
        }
        return result;
    }

    public static Set<List<String>> combinations(List<String> skus, List<Pricing> pricingList) {
        // TODO - maybe a map is the right input to this (SKU -> Pricing)
        // TODO - since each SKU can have one or more pricing objects
        Map<String, List<Pricing>> skuToPricing = new HashMap<>();
        for (Pricing pricing: pricingList) {
            if (!skuToPricing.containsKey(pricing.getSku())) {
                skuToPricing.put(pricing.getSku(), new ArrayList<>());
            }
            skuToPricing.get(pricing.getSku()).add(pricing);
        }

        Set<List<String>> previous = new HashSet<>();
        Set<List<String>> next = new HashSet<>();
        for (int i = skus.size() - 1; i >= 0; i--) {
            String sku = skus.get(i);
            System.out.println("test " + i + " " + sku);

            for (int j = 0; j < skuToPricing.get(sku).size(); j++) {
                if (previous.size() == 0) {
                    Pricing pricing = skuToPricing.get(sku).get(j);
                    List<String> temp = new ArrayList<>();
                    temp.add(pricing.getMerchant().toString() + "_" + pricing.getSku());
                    next.add(temp);
                }
                else {
                    Set<List<String>> temp = deepCopy(previous);
                    for (List<String> subArray : temp) {
                        Pricing pricing = skuToPricing.get(sku).get(j);
                        subArray.add(pricing.getMerchant().toString() + "_" + pricing.getSku());
                    }
                    next.addAll(temp);
                }
            }
            previous = next;
            next = new HashSet<>();
        }
        return previous;
    }
}
