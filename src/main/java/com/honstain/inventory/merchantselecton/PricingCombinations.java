package com.honstain.inventory.merchantselecton;

import com.honstain.inventory.api.Pricing;
import com.honstain.inventory.api.ShipCost;
import org.apache.commons.lang3.tuple.Pair;

import java.util.*;
import java.util.stream.Collectors;

public class PricingCombinations {

    private static Set<SkuSelection> generateCombinations(List<Pricing> pricing, List<ShipCost> shipCosts) {
        Set<String> skus = pricing.stream()
                .map(Pricing::getSku)
                .collect(Collectors.toSet());

        // Each SKU has at least one pricing, and each pricing has at least one ship cost
        Map<String, ShipCost> shipCostMap = shipCosts.stream()
                .collect(Collectors.toMap(ShipCost::getSku, s -> s));
        // WARNING - there isn't just one shipcost per merchant and SKU, a SKU could be part
        // of more than one deal.
        Map<Pair<Integer, String>, ShipCost> shipCostMerchantMap = shipCosts.stream()
                .collect(Collectors.toMap(s -> Pair.of(s.getMerchant(), s.getSku()), s -> s));

        Map<String, Map<Pricing, List<ShipCost>>> data = new HashMap<>();

        Set<SkuSelection> result = new HashSet<>();

        return result;
    }
}
