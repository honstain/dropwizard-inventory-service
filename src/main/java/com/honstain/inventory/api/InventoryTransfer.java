package com.honstain.inventory.api;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.Objects;
import java.util.UUID;

public class InventoryTransfer {
    private UUID id;
    private String fromLocation;
    private String toLocation;
    private String sku;
    private Integer qty;

    public InventoryTransfer() {
        // Jackson deserialization
    }

    public InventoryTransfer(UUID id, String fromLocation, String toLocation, String sku, Integer qty) {
        this.id = id;
        this.fromLocation = fromLocation;
        this.toLocation = toLocation;
        this.sku = sku;
        this.qty = qty;
    }

    @JsonProperty
    public UUID getId() {
        return id;
    }

    @JsonProperty
    public String getFromLocation() {
        return fromLocation;
    }

    @JsonProperty
    public String getToLocation() {
        return toLocation;
    }

    @JsonProperty
    public String getSku() {
        return sku;
    }

    @JsonProperty
    public Integer getQty() {
        return qty;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        InventoryTransfer that = (InventoryTransfer) o;
        return Objects.equals(id, that.id) &&
                Objects.equals(fromLocation, that.fromLocation) &&
                Objects.equals(toLocation, that.toLocation) &&
                Objects.equals(sku, that.sku) &&
                Objects.equals(qty, that.qty);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, fromLocation, toLocation, sku, qty);
    }
}
