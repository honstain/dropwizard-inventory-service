package com.honstain.inventory.api;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.Objects;
import java.util.UUID;

public class Pricing {
    private Integer merchant;
    private String sku;
    private Integer cost;

    public Pricing() {
        // Jackson deserialization
    }

    public Pricing(Integer merchant, String sku, Integer cost) {
        this.merchant = merchant;
        this.sku = sku;
        this.cost = cost;
    }

    @JsonProperty
    public Integer getMerchant() {
        return merchant;
    }

    @JsonProperty
    public String getSku() {
        return sku;
    }

    @JsonProperty
    public Integer getCost() {
        return cost;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Pricing pricing = (Pricing) o;
        return merchant.equals(pricing.merchant) &&
                sku.equals(pricing.sku) &&
                cost.equals(pricing.cost);
    }

    @Override
    public int hashCode() {
        return Objects.hash(merchant, sku, cost);
    }
}
