package com.honstain.inventory.api;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.Objects;
import java.util.UUID;

public class Inventory implements Comparable<Inventory> {
    private UUID id;
    private String location;
    private String sku;
    private Integer qty;

    public Inventory() {
        // Jackson deserialization
    }
    public Inventory(UUID id, String location, String sku, Integer qty) {
        this.id = id;
        this.location = location;
        this.sku = sku;
        this.qty = qty;
    }

    @JsonProperty
    public UUID getId() {
        return id;
    }

    @JsonProperty
    public String getLocation() {
        return location;
    }

    @JsonProperty
    public String getSku() {
        return sku;
    }

    @JsonProperty
    public Integer getQty() {
        return qty;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Inventory that = (Inventory) o;
        return Objects.equals(id, that.id) &&
                Objects.equals(location, that.location) &&
                Objects.equals(sku, that.sku) &&
                Objects.equals(qty, that.qty);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, location, sku, qty);
    }

    @Override
    public int compareTo(Inventory inventory) {
        return id.compareTo(inventory.id);
    }
}
