package com.honstain.inventory.api;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.Objects;
import java.util.UUID;

public class ShipCost {
    private UUID id;
    private Integer merchant;
    private Integer groupId;
    private String sku;
    private Integer cost;

    public ShipCost() {
        // Jackson deserialization
    }

    public ShipCost(UUID id, Integer merchant, Integer groupId, String sku, Integer cost) {
        this.id = id;
        this.merchant = merchant;
        this.groupId = groupId;
        this.sku = sku;
        this.cost = cost;
    }

    @JsonProperty
    public UUID getId() {
        return id;
    }

    @JsonProperty
    public Integer getMerchant() {
        return merchant;
    }

    @JsonProperty
    public Integer getGroupId() {
        return groupId;
    }

    @JsonProperty
    public String getSku() {
        return sku;
    }

    @JsonProperty
    public Integer getCost() {
        return cost;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        ShipCost shipCost = (ShipCost) o;
        return id.equals(shipCost.id) &&
                merchant.equals(shipCost.merchant) &&
                groupId.equals(shipCost.groupId) &&
                sku.equals(shipCost.sku) &&
                cost.equals(shipCost.cost);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, merchant, groupId, sku, cost);
    }
}
