package com.honstain.inventory.db;

import com.honstain.inventory.api.Pricing;
import org.jdbi.v3.core.mapper.RowMapper;
import org.jdbi.v3.core.statement.StatementContext;

import java.sql.ResultSet;
import java.sql.SQLException;


public class PricingMapper implements RowMapper<Pricing> {
    @Override
    public Pricing map(ResultSet rs, StatementContext ctx) throws SQLException {
        return new Pricing(
                rs.getInt("merchant"),
                rs.getString("sku"),
                rs.getInt("cost")
        );
    }
}
