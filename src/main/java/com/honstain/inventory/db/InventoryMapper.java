package com.honstain.inventory.db;

import com.honstain.inventory.api.Inventory;
import org.jdbi.v3.core.mapper.RowMapper;
import org.jdbi.v3.core.statement.StatementContext;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.UUID;

public class InventoryMapper implements RowMapper<Inventory> {
    @Override
    public Inventory map(ResultSet rs, StatementContext ctx) throws SQLException {
        return new Inventory(
                UUID.fromString(rs.getString("id")),
                rs.getString("location"),
                rs.getString("sku"),
                rs.getInt("qty")
        );
    }
}
