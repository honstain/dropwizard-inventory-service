package com.honstain.inventory.db;

public class InventoryOverlord {

    private Chain head;
    private ModificataionRequests requests;

    public InventoryOverlord() {
        this.head = new LockProcessor();
        this.head.setNext(new UpdateProcessor());
        this.requests = new ModificataionRequests();
    }

    public void process() {
        this.head.process(requests);
    }
}
