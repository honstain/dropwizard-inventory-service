package com.honstain.inventory.db;

import com.honstain.inventory.api.Inventory;
import com.honstain.inventory.api.Pricing;
import org.jdbi.v3.sqlobject.config.RegisterRowMapper;
import org.jdbi.v3.sqlobject.customizer.Bind;
import org.jdbi.v3.sqlobject.statement.SqlQuery;
import org.jdbi.v3.sqlobject.statement.SqlUpdate;

import java.util.List;

public interface PricingDao {

    @SqlUpdate(
            "CREATE TABLE IF NOT EXISTS pricing ( " +
            "merchant int not null, " +
            "sku text not null, " +
            "cost int not null, " +
            "created_at timestamp default current_timestamp, " +
            "PRIMARY KEY  (merchant, sku) " +
            ");"
    )
    void createPricingTable();

    @SqlUpdate("TRUNCATE pricing")
    void dropPricingTable();

    @SqlUpdate(
            "INSERT INTO pricing (merchant, sku, cost) " +
            "VALUES (:merchant, :sku, :cost)"
    )
    void insert(
            @Bind("merchant") Integer merchant,
            @Bind("sku") String sku,
            @Bind("cost") Integer cost
    );

    @SqlQuery("SELECT merchant, sku, cost FROM pricing")
    @RegisterRowMapper(PricingMapper.class)
    List<Pricing> selectAll();

    @SqlQuery("SELECT merchant, sku, cost FROM pricing WHERE sku = :sku")
    @RegisterRowMapper(PricingMapper.class)
    List<Pricing> selectBySku(@Bind("sku") String sku);
}
