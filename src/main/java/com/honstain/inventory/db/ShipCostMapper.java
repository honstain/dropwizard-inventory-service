package com.honstain.inventory.db;

import com.honstain.inventory.api.ShipCost;
import org.jdbi.v3.core.mapper.RowMapper;
import org.jdbi.v3.core.statement.StatementContext;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.UUID;


public class ShipCostMapper implements RowMapper<ShipCost> {
    @Override
    public ShipCost map(ResultSet rs, StatementContext ctx) throws SQLException {
        return new ShipCost(
                UUID.fromString(rs.getString("id")),
                rs.getInt("merchant"),
                rs.getInt("ship_group"),
                rs.getString("sku"),
                rs.getInt("ship_cost")
        );
    }
}
