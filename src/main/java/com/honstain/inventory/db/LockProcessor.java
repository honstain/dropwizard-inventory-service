package com.honstain.inventory.db;

public class LockProcessor implements Chain {

    private Chain nextLink;

    @Override
    public void setNext(Chain nextLink) {
        this.nextLink = nextLink;
    }

    @Override
    public void process(ModificataionRequests requests) {
        // Do work
        System.out.println("Lock Processor");

        if (nextLink != null) {
            nextLink.process(requests);
        }
    }
}
