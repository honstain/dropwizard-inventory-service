package com.honstain.inventory.db;

import com.honstain.inventory.api.Inventory;

import java.util.Set;
import java.util.TreeSet;

public class ModificataionRequests {
    private Set<Inventory> decrementRecords = new TreeSet<>();
    private Set<Inventory> incrementRecords = new TreeSet<>();

    public void decrease(Inventory inventory) {
        decrementRecords.add(inventory);
    }

    public void increase(Inventory inventory) {
        incrementRecords.add(inventory);
    }

    public Set<Inventory> getDecrementRecords() {
        return decrementRecords;
    }

    public Set<Inventory> getIncrementRecords() {
        return incrementRecords;
    }
}
