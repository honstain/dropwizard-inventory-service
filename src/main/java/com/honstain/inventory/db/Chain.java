package com.honstain.inventory.db;

public interface Chain {
    void setNext(Chain nextLink);
    void process(ModificataionRequests requests);
}
