package com.honstain.inventory.db;

import com.honstain.inventory.api.ShipCost;
import org.jdbi.v3.sqlobject.config.RegisterRowMapper;
import org.jdbi.v3.sqlobject.customizer.Bind;
import org.jdbi.v3.sqlobject.statement.SqlQuery;
import org.jdbi.v3.sqlobject.statement.SqlUpdate;

import java.util.List;
import java.util.UUID;

public interface ShipCostDao {

    @SqlUpdate(
            "CREATE TABLE IF NOT EXISTS shipcost ( " +
            "id UUID PRIMARY KEY, " +
            "merchant int, " +
            "ship_group int, " +
            "sku text, " +
            "ship_cost int, " +
            "created_at timestamp default current_timestamp " +
            ");"
    )
    void createShipCostTable();

    @SqlUpdate("TRUNCATE shipcost")
    void dropShipCostTable();

    @SqlUpdate(
            "INSERT INTO shipcost (id, merchant, ship_group, sku, ship_cost) " +
            "VALUES (:id, :merchant, :ship_group, :sku, :ship_cost)"
    )
    void insert(
            @Bind("id") UUID id,
            @Bind("merchant") Integer merchant,
            @Bind("ship_group") Integer shipGroup,
            @Bind("sku") String sku,
            @Bind("ship_cost") Integer cost
    );

    @SqlQuery("SELECT id, merchant, ship_group, sku, ship_cost FROM shipcost")
    @RegisterRowMapper(ShipCostMapper.class)
    List<ShipCost> selectAll();

    @SqlQuery("SELECT id, merchant, ship_group, sku, ship_cost FROM shipcost WHERE sku = :sku")
    @RegisterRowMapper(ShipCostMapper.class)
    List<ShipCost> selectBySku(@Bind("sku") String sku);
}
