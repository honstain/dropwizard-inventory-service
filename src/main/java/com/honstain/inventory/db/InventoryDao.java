package com.honstain.inventory.db;

import com.honstain.inventory.api.Inventory;
import org.jdbi.v3.sqlobject.config.RegisterRowMapper;
import org.jdbi.v3.sqlobject.customizer.Bind;
import org.jdbi.v3.sqlobject.statement.SqlQuery;
import org.jdbi.v3.sqlobject.statement.SqlUpdate;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

public interface InventoryDao {

    @SqlUpdate(
            "CREATE TABLE IF NOT EXISTS inventory (\n" +
            "    id UUID PRIMARY KEY,\n" +
            "    location text,\n" +
            "    sku text,\n" +
            "    qty int,\n" +
            "    created_at timestamp default current_timestamp\n" +
            ");"
    )
    void createCycleCountTable();

    @SqlUpdate("TRUNCATE inventory")
    void dropCycleCountTable();

    @SqlUpdate(
            "INSERT INTO inventory (id, location, sku, qty) " +
            "VALUES (:id, :location, :sku, :qty)"
    )
    void insert(
            @Bind("id") UUID id,
            @Bind("location") String location,
            @Bind("sku") String sku,
            @Bind("qty") Integer qty
    );

    @SqlUpdate(
            "UPDATE inventory  " +
            "SET qty = :qty " +
            "WHERE id = :id and location = :location and sku = :sku"
    )
    void updateQty(
            @Bind("id") UUID id,
            @Bind("location") String location,
            @Bind("sku") String sku,
            @Bind("qty") Integer qty
    );

    @SqlQuery("SELECT id, location, sku, qty FROM inventory")
    @RegisterRowMapper(InventoryMapper.class)
    List<Inventory> selectAll();

    @SqlQuery("SELECT id, location, sku, qty FROM inventory WHERE location = :location")
    @RegisterRowMapper(InventoryMapper.class)
    List<Inventory> selectByLocation(@Bind("location") String location);

    @SqlQuery(
            "SELECT id, location, sku, qty " +
            "FROM inventory " +
            "WHERE location = :location and sku = :sku " +
            "FOR UPDATE")
    @RegisterRowMapper(InventoryMapper.class)
    List<Inventory> lockAndSelectByLocationAndSku(
            @Bind("location") String location,
            @Bind("sku") String sku
    );
}
