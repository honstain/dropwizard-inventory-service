package com.honstain.inventory.merchantselection;

import com.honstain.inventory.api.Pricing;
import com.honstain.inventory.api.ShipCost;
import com.honstain.inventory.merchantselecton.BruteForce;
import org.junit.jupiter.api.Test;

import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.Set;

import static org.junit.jupiter.api.Assertions.assertEquals;


class BruteForceTest {

    @Test
    void testCombinationsEmpty() {
        List<String> skus = List.of();
        List<Pricing> priceList = List.of();
        assertEquals(Set.<List<String>>of(), BruteForce.combinations(skus, priceList));
    }

    @Test
    void testCombinationsSingleSkuAndTwoMerchants() {
        List<String> skus = List.of("SKU-01");
        List<Pricing> priceList = List.of(
                new Pricing(1, "SKU-01", 850),
                new Pricing(2, "SKU-01", 950)
        );
        Set<List<String>> expected = Set.of(
                List.of("1_SKU-01"),
                List.of("2_SKU-01")
        );
        assertEquals(expected, BruteForce.combinations(skus, priceList));
    }

    @Test
    void testCombinationsTwoSkuAndTwoMerchants() {
        List<String> skus = List.of("SKU-01", "SKU-02");
        List<Pricing> priceList = List.of(
                new Pricing(1, "SKU-01", 850),
                new Pricing(2, "SKU-01", 900),
                new Pricing(1, "SKU-02", 1000),
                new Pricing(2, "SKU-02", 1050)
        );

        Set<List<String>> expected = Set.of(
                List.of("1_SKU-02", "1_SKU-01"),
                List.of("1_SKU-02", "2_SKU-01"),
                List.of("2_SKU-02", "1_SKU-01"),
                List.of("2_SKU-02", "2_SKU-01")
        );
        assertEquals(expected, BruteForce.combinations(skus, priceList));
    }

    @Test
    void testCombinationsThreeSkuAndTwoMerchants() {
        List<String> skus = List.of("SKU-01", "SKU-02", "SKU-03");
        List<Pricing> priceList = List.of(
                new Pricing(1, "SKU-01", 850),
                new Pricing(2, "SKU-01", 900),
                new Pricing(1, "SKU-02", 1000),
                new Pricing(2, "SKU-02", 1050),
                new Pricing(1, "SKU-03", 2000),
                new Pricing(2, "SKU-03", 2050)
        );

        Set<List<String>> expected = Set.of(
                List.of("1_SKU-03", "1_SKU-02", "1_SKU-01"),
                List.of("1_SKU-03", "1_SKU-02", "2_SKU-01"),
                List.of("1_SKU-03", "2_SKU-02", "1_SKU-01"),
                List.of("1_SKU-03", "2_SKU-02", "2_SKU-01"),
                List.of("2_SKU-03", "1_SKU-02", "1_SKU-01"),
                List.of("2_SKU-03", "1_SKU-02", "2_SKU-01"),
                List.of("2_SKU-03", "2_SKU-02", "1_SKU-01"),
                List.of("2_SKU-03", "2_SKU-02", "2_SKU-01")
        );
        assertEquals(expected, BruteForce.combinations(skus, priceList));
    }

    @Test
    void testCombinationsThreeSkuAndTwoMerchantsNotBalanced() {
        List<String> skus = List.of("SKU-01", "SKU-02", "SKU-03");
        List<Pricing> priceList = List.of(
                new Pricing(1, "SKU-01", 850),
                new Pricing(2, "SKU-01", 900),
                new Pricing(1, "SKU-02", 1000), // SKU-02 is only available at one merchant
                new Pricing(1, "SKU-03", 2000),
                new Pricing(2, "SKU-03", 2050)
        );

        Set<List<String>> expected = Set.of(
                List.of("1_SKU-03", "1_SKU-02", "1_SKU-01"),
                List.of("1_SKU-03", "1_SKU-02", "2_SKU-01"),
                List.of("2_SKU-03", "1_SKU-02", "1_SKU-01"),
                List.of("2_SKU-03", "1_SKU-02", "2_SKU-01")
        );
        assertEquals(expected, BruteForce.combinations(skus, priceList));
    }
}
