package com.honstain.inventory;

import com.honstain.inventory.api.Inventory;
import com.honstain.inventory.db.InventoryDao;
import io.dropwizard.jdbi3.JdbiFactory;
import io.dropwizard.testing.ResourceHelpers;
import io.dropwizard.testing.junit5.DropwizardAppExtension;
import io.dropwizard.testing.junit5.DropwizardExtensionsSupport;
import org.jdbi.v3.core.Jdbi;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;

import javax.ws.rs.client.Entity;
import javax.ws.rs.core.GenericType;
import javax.ws.rs.core.Response;
import java.util.List;
import java.util.UUID;

import static org.junit.jupiter.api.Assertions.assertEquals;


@ExtendWith(DropwizardExtensionsSupport.class)
class IntegrationTest {
    private static final String CONFIG_PATH = ResourceHelpers.resourceFilePath("config.yml");

    static final DropwizardAppExtension<InventoryServiceConfiguration> DROPWIZARD =
            new DropwizardAppExtension<>(InventoryServiceApplication.class, CONFIG_PATH);

    private static InventoryDao inventoryDao;

    @BeforeAll
    static void setUpAll() {
        Jdbi jdbi = new JdbiFactory().build(
                DROPWIZARD.getEnvironment(),
                DROPWIZARD.getConfiguration().getDataSourceFactory(),
                "postgresql-test"
        );
        inventoryDao = jdbi.onDemand(InventoryDao.class);
        inventoryDao.createCycleCountTable();
    }

    @BeforeEach
    void setup() {
        inventoryDao.dropCycleCountTable();
    }

    @Test
    void testGetCycleCounts() {
        final Inventory newInventory = new Inventory(UUID.randomUUID(), "LOC-01", "SKU-01", 4);

        inventoryDao.insert(newInventory.getId(), newInventory.getLocation(), newInventory.getSku(), newInventory.getQty());

        final List<Inventory> expected = List.of(newInventory);
        final List<Inventory> inventories = DROPWIZARD.client()
                .target("http://localhost:" + DROPWIZARD.getLocalPort() + "/")
                .request()
                .get(new GenericType<>() {});
        assertEquals(expected, inventories);
    }

    @Test
    void testCreateCycleCounts() {
        final Inventory newInventory = new Inventory(UUID.randomUUID(), "LOC-01", "SKU-01", 4);

        final Response response = DROPWIZARD.client()
                .target("http://localhost:" + DROPWIZARD.getLocalPort() + "/")
                .request()
                .post(Entity.json(newInventory));
        assertEquals(204, response.getStatus());
    }

    @Test
    void testCreateCycleCountsAndGetCycleCounts() {
        final Inventory newInventory = new Inventory(UUID.randomUUID(), "LOC-01", "SKU-01", 4);

        final Response response = DROPWIZARD.client()
                .target("http://localhost:" + DROPWIZARD.getLocalPort() + "/")
                .request()
                .post(Entity.json(newInventory));
        assertEquals(204, response.getStatus());

        final List<Inventory> inventories = DROPWIZARD.client()
                .target("http://localhost:" + DROPWIZARD.getLocalPort() + "/")
                .request()
                .get(new GenericType<>() {});

        assertEquals(List.of(newInventory), inventories);
    }
}


