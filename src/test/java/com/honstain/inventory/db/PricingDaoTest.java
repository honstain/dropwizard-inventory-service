package com.honstain.inventory.db;

import com.honstain.inventory.InventoryServiceApplication;
import com.honstain.inventory.InventoryServiceConfiguration;
import com.honstain.inventory.api.Inventory;
import com.honstain.inventory.api.Pricing;
import io.dropwizard.jdbi3.JdbiFactory;
import io.dropwizard.testing.ResourceHelpers;
import io.dropwizard.testing.junit5.DropwizardAppExtension;
import io.dropwizard.testing.junit5.DropwizardExtensionsSupport;
import org.jdbi.v3.core.Jdbi;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;

import java.util.List;
import java.util.UUID;

import static org.junit.jupiter.api.Assertions.assertEquals;

@ExtendWith(DropwizardExtensionsSupport.class)
class PricingDaoTest {
    private static final String CONFIG_PATH = ResourceHelpers.resourceFilePath("config.yml");

    static final DropwizardAppExtension<InventoryServiceConfiguration> DROPWIZARD =
            new DropwizardAppExtension<>(InventoryServiceApplication.class, CONFIG_PATH);

    private static PricingDao pricingDao;

    @BeforeAll
    static void setUpAll() {
        Jdbi jdbi = new JdbiFactory().build(
                DROPWIZARD.getEnvironment(),
                DROPWIZARD.getConfiguration().getDataSourceFactory(),
                "postgresql-test"
        );
        pricingDao = jdbi.onDemand(PricingDao.class);
        pricingDao.createPricingTable();
    }

    @BeforeEach
    void setup() {
        pricingDao.dropPricingTable();
    }

    @Test
    void testSelectAllEmpty() {
        assertEquals(List.of(), pricingDao.selectAll());
    }

    @Test
    void testInsertAndSelectAll() {
        final Pricing pricing = new Pricing(1, "SKU-01", 8000);
        pricingDao.insert(pricing.getMerchant(), pricing.getSku(), pricing.getCost());
        assertEquals(List.of(pricing), pricingDao.selectAll());
    }

    @Test
    void testInsertAndSelectBySku() {
        final Pricing pricing1 = new Pricing(1, "SKU-01", 8000);
        final Pricing pricing2 = new Pricing(1, "SKU-02", 10000);
        pricingDao.insert(pricing1.getMerchant(), pricing1.getSku(), pricing1.getCost());
        pricingDao.insert(pricing2.getMerchant(), pricing2.getSku(), pricing2.getCost());
        assertEquals(List.of(pricing1), pricingDao.selectBySku("SKU-01"));
    }
}
