package com.honstain.inventory.db;

import com.honstain.inventory.InventoryServiceApplication;
import com.honstain.inventory.InventoryServiceConfiguration;
import com.honstain.inventory.api.Inventory;
import io.dropwizard.jdbi3.JdbiFactory;
import io.dropwizard.testing.ResourceHelpers;
import io.dropwizard.testing.junit5.DropwizardAppExtension;
import io.dropwizard.testing.junit5.DropwizardExtensionsSupport;
import org.jdbi.v3.core.Jdbi;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;

import java.util.List;
import java.util.UUID;

import static org.junit.jupiter.api.Assertions.assertEquals;

@ExtendWith(DropwizardExtensionsSupport.class)
class InventoryDaoTest {
    private static final String CONFIG_PATH = ResourceHelpers.resourceFilePath("config.yml");

    static final DropwizardAppExtension<InventoryServiceConfiguration> DROPWIZARD =
            new DropwizardAppExtension<>(InventoryServiceApplication.class, CONFIG_PATH);

    private static InventoryDao inventoryDao;

    @BeforeAll
    static void setUpAll() {
        Jdbi jdbi = new JdbiFactory().build(
                DROPWIZARD.getEnvironment(),
                DROPWIZARD.getConfiguration().getDataSourceFactory(),
                "postgresql-test"
        );
        inventoryDao = jdbi.onDemand(InventoryDao.class);
        inventoryDao.createCycleCountTable();
    }

    @BeforeEach
    void setup() {
        inventoryDao.dropCycleCountTable();
    }

    @Test
    void testSelectAllEmpty() {
        assertEquals(List.of(), inventoryDao.selectAll());
    }

    @Test
    void testInsertAndSelectAll() {
        final Inventory count = new Inventory(UUID.randomUUID(), "LOC-01", "SKU-01", 4);

        inventoryDao.insert(
                count.getId(),
                count.getLocation(),
                count.getSku(),
                count.getQty()
        );
        assertEquals(List.of(count), inventoryDao.selectAll());
    }
}
