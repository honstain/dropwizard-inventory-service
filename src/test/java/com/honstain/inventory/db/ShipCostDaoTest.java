package com.honstain.inventory.db;

import com.honstain.inventory.InventoryServiceApplication;
import com.honstain.inventory.InventoryServiceConfiguration;
import com.honstain.inventory.api.Inventory;
import com.honstain.inventory.api.ShipCost;
import io.dropwizard.jdbi3.JdbiFactory;
import io.dropwizard.testing.ResourceHelpers;
import io.dropwizard.testing.junit5.DropwizardAppExtension;
import io.dropwizard.testing.junit5.DropwizardExtensionsSupport;
import org.jdbi.v3.core.Jdbi;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;

import java.util.List;
import java.util.UUID;

import static org.junit.jupiter.api.Assertions.assertEquals;

@ExtendWith(DropwizardExtensionsSupport.class)
class ShipCostDaoTest {
    private static final String CONFIG_PATH = ResourceHelpers.resourceFilePath("config.yml");

    static final DropwizardAppExtension<InventoryServiceConfiguration> DROPWIZARD =
            new DropwizardAppExtension<>(InventoryServiceApplication.class, CONFIG_PATH);

    private static ShipCostDao shipCostDao;

    @BeforeAll
    static void setUpAll() {
        Jdbi jdbi = new JdbiFactory().build(
                DROPWIZARD.getEnvironment(),
                DROPWIZARD.getConfiguration().getDataSourceFactory(),
                "postgresql-test"
        );
        shipCostDao = jdbi.onDemand(ShipCostDao.class);
        shipCostDao.createShipCostTable();
    }

    @BeforeEach
    void setup() {
        shipCostDao.dropShipCostTable();
    }

    @Test
    void testSelectAllEmpty() {
        assertEquals(List.of(), shipCostDao.selectAll());
    }

    @Test
    void testInsertAndSelectAll() {
        final ShipCost shipCost = new ShipCost(UUID.randomUUID(), 1, 1, "SKU-01", 850);

        shipCostDao.insert(shipCost.getId(), shipCost.getMerchant(), shipCost.getGroupId(), shipCost.getSku(), shipCost.getCost());
        assertEquals(List.of(shipCost), shipCostDao.selectAll());
    }

    @Test
    void testInsertAndSelectBySku() {
        final ShipCost shipCost1 = new ShipCost(UUID.randomUUID(), 1, 1, "SKU-01", 850);
        final ShipCost shipCost2 = new ShipCost(UUID.randomUUID(), 1, 2, "SKU-02", 750);

        shipCostDao.insert(shipCost1.getId(), shipCost1.getMerchant(), shipCost1.getGroupId(), shipCost1.getSku(), shipCost1.getCost());
        shipCostDao.insert(shipCost2.getId(), shipCost2.getMerchant(), shipCost2.getGroupId(), shipCost2.getSku(), shipCost2.getCost());
        assertEquals(List.of(shipCost2), shipCostDao.selectBySku("SKU-02"));
    }
}
