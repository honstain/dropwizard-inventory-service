# Inventory Service



How to start the inventory service application
---

1. Run `mvn clean install` to build your application
1. Start application with `java -jar target/inventory-service-1.0-SNAPSHOT.jar server config.yml`
1. To check that your application is running enter url `http://localhost:8080`

Health Check
---

To see your applications health enter url `http://localhost:8081/healthcheck`


Create DB
---
Instructions for PostgreSQL 11.5

`createuser --interactive`
* Create user 'dropwiz-inventory'
`createdb dropwiz-inventory`
`alter user "dropwiz-inventory" password 'dev';`

```
CREATE TABLE IF NOT EXISTS inventory (
id UUID PRIMARY KEY,
location text,
sku text,
qty int,
created_at timestamp default current_timestamp
);


CREATE TABLE IF NOT EXISTS pricing (
merchant int not null,
sku text not null,
cost int not null,
created_at timestamp default current_timestamp,
PRIMARY KEY  (merchant, sku)
);

CREATE TABLE IF NOT EXISTS shipcost (
id UUID PRIMARY KEY,
merchant int,
ship_group int,
sku text,
ship_cost int,
created_at timestamp default current_timestamp
);

SELECT * FROM inventory;

SELECT * FROM pricing;
SELECT * FROM shipcost;

INSERT INTO public.pricing (merchant, sku, cost, created_at) VALUES (1, 'SKU-01', 1500, '2019-11-11 15:27:24.000000');
INSERT INTO public.pricing (merchant, sku, cost, created_at) VALUES (1, 'SKU-02', 1500, '2019-11-11 23:27:41.761640');
INSERT INTO public.pricing (merchant, sku, cost, created_at) VALUES (1, 'SKU-04', 3300, '2019-11-11 23:29:16.338129');
INSERT INTO public.pricing (merchant, sku, cost, created_at) VALUES (2, 'SKU-01', 1500, '2019-11-11 23:29:16.338129');
INSERT INTO public.pricing (merchant, sku, cost, created_at) VALUES (2, 'SKU-02', 1600, '2019-11-11 23:29:16.338129');
INSERT INTO public.pricing (merchant, sku, cost, created_at) VALUES (2, 'SKU-04', 3350, '2019-11-11 23:29:16.338129');
INSERT INTO public.pricing (merchant, sku, cost, created_at) VALUES (3, 'SKU-02', 1800, '2019-11-11 23:29:16.338129');
INSERT INTO public.pricing (merchant, sku, cost, created_at) VALUES (3, 'SKU-03', 1900, '2019-11-11 23:29:16.338129');
INSERT INTO public.pricing (merchant, sku, cost, created_at) VALUES (3, 'SKU-04', 3100, '2019-11-11 23:29:16.338129');
INSERT INTO public.pricing (merchant, sku, cost, created_at) VALUES (4, 'SKU-01', 1200, '2019-11-11 23:29:53.376976');
INSERT INTO public.pricing (merchant, sku, cost, created_at) VALUES (4, 'SKU-03', 1600, '2019-11-11 23:29:53.376976');
INSERT INTO public.pricing (merchant, sku, cost, created_at) VALUES (4, 'SKU-04', 2800, '2019-11-11 23:29:53.376976');

INSERT INTO shipcost (id, merchant, ship_group, sku, ship_cost)
VALUES
       ('36246a9b-5349-4078-b915-760086b0f76c', 1, 1, 'SKU-01', 750),
       ('d3d6debe-f769-4d2a-a9dc-be92ba75af46', 1, 1, 'SKU-02', 750),
       ('45b2e885-a569-4b3d-b7d6-a56a415b18d0', 1, 2, 'SKU-04', 750),

       ('3331283a-1c7b-484c-945c-0d848ab37ba0', 2, 3, 'SKU-01', 850),
       ('02b07dba-b65c-41bd-945f-39c738cdc3e7', 2, 3, 'SKU-01', 850),
       ('3875b4a8-57fb-4149-b95c-184d86b95d9b', 2, 3, 'SKU-01', 850),

       ('4e32ae0c-fd6d-44c9-b333-dbe4f2994f37', 3, 4, 'SKU-02', 350),
       ('91f61765-f237-4349-a9d9-f96b1b22e192', 3, 5, 'SKU-03', 350),
       ('bd209302-1eb1-4b04-8177-4f1551a35a4e', 3, 6, 'SKU-04', 350),
       ('7e96484c-bf6b-41fc-9fc1-83c941d7943c', 3, 7, 'SKU-03', 450),
       ('00126593-0c80-4cbd-b042-41bb099b2a3a', 3, 7, 'SKU-04', 450),

       ('ead85a54-9636-4e63-81d6-04697c8ff1a9', 4, 8, 'SKU-01', 550),
       ('6b42e5b7-93b3-4167-8dfe-535ae921c0a2', 4, 9, 'SKU-03', 550),
       ('c0709d29-2d52-46c6-865c-c7fcce85f692', 4, 10, 'SKU-04', 550);
```